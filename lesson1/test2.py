def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum

print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))


'''output:
20
12
22
explain: sum(x, y) return sum = x + y but if sum has value between 15 and 20, it return 20'''
