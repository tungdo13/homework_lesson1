import textwrap

def wrap_string(input_data, W):
	wrapper = textwrap.TextWrapper(width=W) 
	word_list = wrapper.wrap(text=input_data)
	return word_list


if __name__ == '__main__':
 	input_data = "1 con vit xoe ra 2 cai canh no keu rang quac quac quac quac quac quac"
 	W = 10
 	for line in wrap_string(input_data, W):
 		print(line)