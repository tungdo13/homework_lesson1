def take_number_in_two_list(list1, list2):
	result = [number for number in set(list1) if number in set(list2)]
	return result

if __name__ == '__main__':
	list1 = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
	list2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
	print(take_number_in_two_list(list1, list2))