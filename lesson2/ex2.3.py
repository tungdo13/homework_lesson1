def take_sorted_keys(my_dict):
	list_ =  list(my_dict.items())
	list_.sort(key = lambda list_ : list_[1])
	result = [item[0] for item in list_]
	return result


if __name__ == '__main__':
	my_dict = {'a':9, 'b':1, 'c':12, 'd':7}
	print(take_sorted_keys(my_dict))