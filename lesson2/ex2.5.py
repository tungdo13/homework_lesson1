def is_palindrome(input_data):
	if len(input_data) >= 2:
		input_data = input_data.strip().lower()
		if input_data == input_data[::-1]:
			print("{} is a is_palindrome".format(input_data))
		else:
			pass
if __name__ == '__main__':
	input_data = "abcddcba"
	is_palindrome(input_data)