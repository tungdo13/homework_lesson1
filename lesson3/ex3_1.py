import numpy as np
import random

def remove_number(numpy_array) :
	list_ = list(numpy_array)
	for number in list_ :
		if number > 3 and number < 8 :
			list_.remove(number)
	return list_


if __name__ == '__main__':
	numpy_array = np.random.randint(1, 20, 10)
	print(remove_number(numpy_array))



