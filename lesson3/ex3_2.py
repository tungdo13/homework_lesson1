import numpy as np
import random

def main(np_array):
	np_array[np_array.argmax()] = 0
	return np_array


if __name__ == '__main__':
	np_array = np.random.randn(10)
	main(np_array)