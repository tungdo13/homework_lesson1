import numpy as np

def main(array1, array2) :
	for number in array1 :
		if number in array2 :
			print(number)


if __name__ == '__main__':
	array1 = np.array([1, 2 ,4 ,5])
	array2 = np.array([1, 3, 5])
	main(array1, array2)