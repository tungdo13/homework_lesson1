import numpy as np

def main(array):
	return array[::-1]


if __name__ == '__main__':
	array = np.arange(10)
	print(main(array))